const express = require('express');
const router = express.Router();

const request = require('request');
const fs = require('fs');
const mkdirp = require('mkdirp');
const path = require('path');
const utilities = require('./utilities');

function spider(url, callback) {
    const filename = utilities.urlToFilename(url);
    fs.access(filename, err => {
        if (err) {
            console.log(`Downloading ${url}`);
            request(url, (error, response, body) => {
                if(error) {
                    callback(error);
                } else {
                    mkdirp(path.dirname(filename), err => {
                        if(err) {
                            callback(err);
                        } else {
                            fs.writeFile(filename, body, err => {
                                if(err) {
                                    callback(err);
                                } else {
                                    callback(null, filename, true);
                                }
                            });
                        }
                    });
                }
            });
        } else {
            callback(null, filename, false);
        }
    });
}

spider('http://example.url', (err, filename, downloaded) => {
    if(err) {
        console.log(err);
    } else if(downloaded){
        console.log(`Completed the download of "${filename}"`);
    } else {
        console.log(`"${filename}" was already downloaded`);
    }
});


module.exports = router;
